#!/bin/bash

set -euo pipefail

export METHOD=${METHOD:-podman}
export MIGRATE=${MIGRATE:-1}
export NO_INSTALL=${NO_INSTALL:-1}
export ART_VERSION=${ART_VERSION:-7}
export RHEL_VERSION=${RHEL_VERSION:-88}
export RESERVED_MEMORY=${RESERVED_MEMORY:-31744}
export MASTER_KEY=${MASTER_KEY:-""}
export JOIN_KEY=${JOIN_KEY:-""}

reboot_vagrant () {
  cd "rhel${RHEL_VERSION}"
  vagrant reload node-2 # halt (shutdown) + up (without provisioning)
  cd ..
}

test_artifactory-node-1 () {
  cd "rhel${RHEL_VERSION}"
  vagrant ssh node-1 -c 'bash -s' <../wait_for_artifactory.sh
  vagrant ssh node-1 -c 'bash -s' <../test.sh
  cd ..
}
test_artifactory-node-2 () {
  cd "rhel${RHEL_VERSION}"
  vagrant ssh node-2 -c 'bash -s' <../wait_for_artifactory.sh
  vagrant ssh node-2 -c 'bash -s' <../test.sh
  cd ..
}
grep_artifactory-node-2_logs () {
  cd "rhel${RHEL_VERSION}"
  vagrant ssh node-2 -c 'podman logs artifactory' | grep "Application could not be initialized: Found active HA servers in DB, OSS is not supported in by active HA environment."
  echo "HA not supported for OSS license -> this is considered a success!"
  cd ..
}

deploy_artifactory () {
  cd ansible
  TARGET=$1 ./deploy.sh
  cd ..
}

echo "Perfoming deployment 1 with ${METHOD}..."
deploy_artifactory node-1

echo "Test deploy..."
test_artifactory-node-1

echo "Perfoming deployment 2 with ${METHOD}..."
deploy_artifactory node-2

# HA not supported on OSS license -> expect failure -> handle by testing logs
test_artifactory-node-2 || grep_artifactory-node-2_logs
