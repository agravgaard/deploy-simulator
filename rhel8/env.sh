#!/bin/sh
# Bullfrog
export AGENT_EXPOSE_PORT=8181
export BULLFROG_ADDRESS=172.26.0.4
export UI_EXPOSE_PORT=4000
export CASSANDRA_DATA_MOUNT=./cassandra-data
# MS SQL
export DATABASE_NAME=ESW_Artifactory_LOCAL
export MSSQL_ADDRESS=172.26.0.2
export MSSQL_PORT=1433
export MSSQL_SA_PASSWORD=terriblepassw0rd.
export SQL_INSTALL_USER=artifactory
export SQL_INSTALL_USER_PASSWORD=anotherterriblepassw0rd.
