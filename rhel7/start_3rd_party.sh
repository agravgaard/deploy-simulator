#!/bin/bash

# For Bullfrog
export AGENT_EXPOSE_PORT=${AGENT_EXPOSE_PORT:-8181}
export BULLFROG_ADDRESS=${BULLFROG_ADDRESS:-127.0.0.1}
export CASSANDRA_DATA_MOUNT=${CASSANDRA_DATA_MOUNT:-./cassandra-data}
export UI_EXPOSE_PORT=${UI_EXPOSE_PORT:-4000}

# For MS SQL
export MSSQL_SA_PASSWORD=${MSSQL_SA_PASSWORD:-"terriblepassw0rd."}
export MSSQL_PORT=${MSSQL_PORT:-1433}
export MSSQL_ADDRESS=${MSSQL_ADDRESS:-127.0.0.1}

my_ip=$(ip route get 1.2.3.4 | awk '{ print $7 }')
export NFS_ADDRESS=${NFS_ADDRESS:-$my_ip}

docker-compose up -d

# 127.0.0.1 doesn't seem to be reachable from inside libvirt, so get the "real" addresses:
BULLFROG_ADDRESS=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' bullfrog)
export BULLFROG_ADDRESS
MSSQL_ADDRESS=$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' mssql)
export MSSQL_ADDRESS

function sqlcmd() {
  docker exec mssql /opt/mssql-tools/bin/sqlcmd \
    -S localhost \
    -U SA \
    -P "$MSSQL_SA_PASSWORD" \
    "$@"
}

# Connect to server and get the version:
counter=1
errstatus=1
while [ $counter -le 5 ] && [ $errstatus = 1 ]; do
  echo Waiting for SQL Server to start...
  sleep 5s
  sqlcmd \
    -S localhost \
    -U SA \
    -P "$MSSQL_SA_PASSWORD" \
    -Q "SELECT @@VERSION" # 2>/dev/null
  errstatus=$?
  ((counter++))
done
# Display error if connection failed:
if [ $errstatus = 1 ]; then
  echo "Cannot connect to SQL Server, installation aborted"
  exit $errstatus
fi

export MSSQL_COLLATION="Latin1_General_CS_AI"
export DATABASE_NAME="ESW_Artifactory_LOCAL"
export SQL_INSTALL_USER="artifactory"
export SQL_INSTALL_USER_PASSWORD="anotherterriblepassw0rd."

# New database creation:
echo "CREATE DATABASE $DATABASE_NAME COLLATE $MSSQL_COLLATION"
sqlcmd \
  -Q "CREATE DATABASE $DATABASE_NAME COLLATE $MSSQL_COLLATION"

# New user creation:
echo "CREATE LOGIN [$SQL_INSTALL_USER] WITH PASSWORD=N'$SQL_INSTALL_USER_PASSWORD', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=ON, CHECK_POLICY=ON; ALTER SERVER ROLE [sysadmin] ADD MEMBER [$SQL_INSTALL_USER]"
sqlcmd \
  -Q "CREATE LOGIN [$SQL_INSTALL_USER] WITH PASSWORD=N'$SQL_INSTALL_USER_PASSWORD', DEFAULT_DATABASE=[$DATABASE_NAME], CHECK_EXPIRATION=ON, CHECK_POLICY=ON; ALTER SERVER ROLE [sysadmin] ADD MEMBER [$SQL_INSTALL_USER]"

echo "USE $DATABASE_NAME; CREATE USER [$SQL_INSTALL_USER] FOR LOGIN [$SQL_INSTALL_USER]"
sqlcmd \
  -Q "USE $DATABASE_NAME; CREATE USER [$SQL_INSTALL_USER] FOR LOGIN [$SQL_INSTALL_USER]"

# Grant access to user:
echo "USE $DATABASE_NAME; GRANT ALL PRIVILEGES ON DATABASE::$DATABASE_NAME TO [$SQL_INSTALL_USER]"
sqlcmd \
  -Q "USE $DATABASE_NAME; GRANT ALL PRIVILEGES ON DATABASE::$DATABASE_NAME TO [$SQL_INSTALL_USER]"

# Export shared enviroment variables
{
  echo "#!/bin/sh"
  echo "# Bullfrog"
  echo "export AGENT_EXPOSE_PORT=$AGENT_EXPOSE_PORT"
  echo "export BULLFROG_ADDRESS=$BULLFROG_ADDRESS"
  echo "export UI_EXPOSE_PORT=$UI_EXPOSE_PORT"
  echo "export CASSANDRA_DATA_MOUNT=$CASSANDRA_DATA_MOUNT"

  echo "# MS SQL"
  echo "export DATABASE_NAME=$DATABASE_NAME"
  echo "export MSSQL_ADDRESS=$MSSQL_ADDRESS"
  echo "export MSSQL_PORT=$MSSQL_PORT"
  echo "export MSSQL_SA_PASSWORD=$MSSQL_SA_PASSWORD"
  echo "export SQL_INSTALL_USER=$SQL_INSTALL_USER"
  echo "export SQL_INSTALL_USER_PASSWORD=$SQL_INSTALL_USER_PASSWORD"

  echo "# NFS server"
  echo "export NFS_ADDRESS=$NFS_ADDRESS"
} >env.sh
