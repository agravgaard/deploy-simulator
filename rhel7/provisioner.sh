#!/bin/bash

# Make yum usable
cat <<EOF >/etc/yum.repos.d/centos1.repo
[centos]
name=CentOS-7
baseurl=http://ftp.heanet.ie/pub/centos/7/os/x86_64/
enabled=1
gpgcheck=1
gpgkey=http://ftp.heanet.ie/pub/centos/7/os/x86_64/RPM-GPG-KEY-CentOS-7
EOF

yum update -y
yum install -y unzip java-1.8.0-openjdk python3 nfs-utils
# java-1.8.0-openjdk is a dependency of artifactory<7
# Artifactory 7+ comes with Java bundled.
# Python 3+ is needed for docker-compose with ansible

# Load shared environment variables (MS SQL credentials)
# shellcheck source=/dev/null
. /tmp/env.sh

# Install artifactory
export JFROG_HOME=/opt/jfrog
mkdir -p $JFROG_HOME
cd $JFROG_HOME || exit 4
export ARTIFACTORY_VERSION=6.18.5 # closest to 6.18.1
# wget "https://releases.jfrog.io/artifactory/bintray-artifactory/org/artifactory/oss/jfrog-artifactory-oss/$ARTIFACTORY_VERSION/jfrog-artifactory-oss-$ARTIFACTORY_VERSION-linux.tar.gz"
# tar xvf jfrog-artifactory-oss-$ARTIFACTORY_VERSION-linux.tar.gz
curl -SL -o jfrog-artifactory-oss-$ARTIFACTORY_VERSION.zip "https://releases.jfrog.io/artifactory/bintray-artifactory/org/artifactory/oss/jfrog-artifactory-oss/$ARTIFACTORY_VERSION/jfrog-artifactory-oss-$ARTIFACTORY_VERSION.zip"
unzip jfrog-artifactory-oss-$ARTIFACTORY_VERSION.zip
mv artifactory-oss* artifactory
export ARTIFACTORY_HOME=$JFROG_HOME/artifactory/

# Bullfrog installation: https://github.com/thefreebit/bullfrog/wiki/Central-Collector-Installation#agent-installation
mkdir -p $ARTIFACTORY_HOME/bullfrog
# Bintray has been sunset and there doesn't seem to be an alternative download location...
cd $ARTIFACTORY_HOME/bullfrog || exit 4
tar xvzf /tmp/bullfrog.tar.gz
cd $JFROG_HOME || exit 4

echo "agent.id=prod::$BULLFROG_ADDRESS" >$ARTIFACTORY_HOME/bullfrog/bullfrog.properties
echo "collector.address=http://$BULLFROG_ADDRESS:$AGENT_EXPOSE_PORT" >>$ARTIFACTORY_HOME/bullfrog/bullfrog.properties

# Configure artifactory to use MS MQL
curl -SL -o mssql-jdbc.tar.gz "https://go.microsoft.com/fwlink/?linkid=2183223"
tar xvzf mssql-jdbc.tar.gz
mv sqljdbc_9.4/enu/mssql-jdbc-9.4.1.jre8.jar $ARTIFACTORY_HOME/tomcat/lib/
chmod +r $ARTIFACTORY_HOME/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar
rm -Rf ./sqljdbc_9.4
cat <<EOF >$ARTIFACTORY_HOME/etc/db.properties
type=mssql
driver=com.microsoft.sqlserver.jdbc.SQLServerDriver
url=jdbc:sqlserver://$MSSQL_ADDRESS:$MSSQL_PORT;databaseName=$DATABASE_NAME;sendStringParametersAsUnicode=false;applicationName=Artifactory
username=$SQL_INSTALL_USER
password=$SQL_INSTALL_USER_PASSWORD
EOF

# Gigabytes of reserved memory - 2
JAVA_MEM=$((${RESERVED_MEMORY::-3} - 3))

export ARTIFACTORY_PID=/var/run/artifactory.pid
export ARTIFACTORY_USER=cs-ws-s-artifactory
export ARTIFACTORY_GROUP=cs_unix_stdgroup
export JAVA_HOME=/etc/alternatives/jre_1.8.0
export JAVA_OPTIONS="-javaagent:$ARTIFACTORY_HOME/bullfrog/glowroot-agent-0.10.12.jar -server -Xms${JAVA_MEM}g -Xmx${JAVA_MEM}g -Xss256k -XX:+UseG1GC -XX:OnOutOfMemoryError='kill -9 %p'"
export JAVA_OPTIONS="$JAVA_OPTIONS -Djruby.compile.invokedynamic=false -Dfile.encoding=UTF8 -Dartdist=zip -Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true -Djava.security.egd=file:/dev/./urandom"
export TOMCAT_HOME=$ARTIFACTORY_HOME/tomcat

{
  echo "ARTIFACTORY_HOME=$ARTIFACTORY_HOME"
  echo "ARTIFACTORY_PID=$ARTIFACTORY_PID"
  echo "ARTIFACTORY_USER=$ARTIFACTORY_USER"
  echo "JAVA_HOME=$JAVA_HOME"
  echo "JAVA_OPTIONS=\"$JAVA_OPTIONS\""
  echo "TOMCAT_HOME=$TOMCAT_HOME"
} >>$ARTIFACTORY_HOME/etc/default

# It seems these keys will be created automatically
# openssl rand -hex 32 > artifactory/var/etc/security/master.key
# chown artifactory artifactory/var/etc/security/master.key
# openssl rand -hex 32 > artifactory/var/etc/security/join.key
# chown artifactory artifactory/var/etc/security/join.key

# $JFROG_HOME/artifactory/app/bin/installService.sh
$ARTIFACTORY_HOME/bin/installService.sh $ARTIFACTORY_USER $ARTIFACTORY_GROUP

chown -R $ARTIFACTORY_USER:$ARTIFACTORY_GROUP $JFROG_HOME

# Mount the NFS
systemctl enable nfs-server
systemctl start nfs-server
mkdir -p /data1
echo "${NFS_ADDRESS}:/srv/nfs3/data  /data1  nfs  rw,fg,hard,vers=3,nointr,timeo=600,tcp,lookupcache=none  0 0" >>/etc/fstab
mount /data1

# Configure data storage (now that we have a user to give permissions to)
grep Dir /tmp/binarystore.xml | sed -rn 's/.+Dir>(.+)<\/.+/\1/p' >/tmp/datastores.txt
mv /tmp/binarystore.xml $ARTIFACTORY_HOME/etc/binarystore.xml
while read -r p; do
  echo "$p"
  mkdir -p "$p"
  chown $ARTIFACTORY_USER:$ARTIFACTORY_GROUP "$p"
  chmod 700 "$p"
done </tmp/datastores.txt

systemctl enable artifactory.service

function print_logs {
  echo "********************START LOG*********************"
  journalctl -xe -n 50 --no-pager
  echo "*********************END LOG**********************"
  echo "********************START TOMCAT LOG*********************"
  cat $TOMCAT_HOME/logs/*
  echo "**********************END TOMCAT LOG*********************"
  exit 69
}

systemctl start artifactory.service || print_logs
