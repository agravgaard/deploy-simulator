# Deploy Simulator

## Usage

All of the commands below inside the `./artifactory` directory.

### Start
Spin up Red Hat environment with Artifactory provisioned:
```
./start_3rd_party.sh
vagrant up
```

`./start_3rd_party.sh` starts the bullfrog collector with a casandra instance, and a Microsoft SQL database using docker compose.
Additionally, it sets up the SQL database and creates a user for artifactory. Environment variables are then saved in `./env.sh`.

By default Vagrant uses 30GB of memory (2GB less, i.e. 28GB will be used for
atifactory) and `libvirt`. This can be specified with the environment variable:
`RESERVED_MEMORY` and the `--provisioner` flag. E.g.:
```
RESERVED_MEMORY=4096 vagrant up --provisioner virtualbox
```
For 4GB, then only 2GB will be reserved for Artifactory (which may not be enough).

### SSH
```
vagrant ssh
```
Setup "real" ssh:
```
vagrant ssh-config >> ~/.ssh/config
```
You'd probably want to edit `~/.ssh/config` afterwards and change the Hostname "default" into something more appropriate.

### Clean up
Tear it down:
```
vagrant destroy
./stop_3rd_party.sh
```

## Artifactory deployments with ansible

**See [ansible/README.md](ansible/README.md)**


## Setup Gitlab Runner for this project

This assumes you have [libvirt](https://wiki.archlinux.org/title/libvirt) and [gitlab-runner](https://docs.gitlab.com/runner/install/linux-manually.html) installed on the system already.

Go to Settings -> CI/CD -> Runners. Copy the registration token and run:
```
sudo gitlab-runner register --url https://gitlab.com/ --registration-token $REGISTRATION_TOKEN
```
Set it up with the tags `linux`, `shell` and `libvirt`, as a shell (I have not had success with docker, yet).

Additionally, for nginx test we'll need to add the below entry to `/etc/hosts`:
```
127.0.0.1   esw-artifactory.net
```

### For libvirt

Add the gitlab-runner to the libvirt and docker group:
```
sudo usermod -aG libvirt gitlab-runner
sudo usermod -aG docker gitlab-runner
```
Uncomment the following lines in `/etc/libvirt/libvirtd.conf`
```
unix_sock_group = "libvirt"
unix_sock_rw_perms = "0770"
```

Install the vagrant-libvirt plugin for the gitlab-runner user:
```
sudo -u gitlab-runner vagrant plugin install vagrant-libvirt
```

### For the NFS server

Add a line similar to the below in your `/etc/exports`:
```
/srv/nfs3        192.168.0.0/16(rw,sync,no_root_squash)
```
Make sure the directory exists with a `data` subdirectory, i.e.:
```
mkdir -p /srv/nfs3/data
```
Then run `sudo exportfs -arv` to start the NFS server.

(This will need nfs to be installed on the system)

## Generate Bullfrog agent archive

Make sure you have maven installed locally.

From the repo directory run:
```
git submodule update --init --recursive
./build_bullfrog.sh
```

## Note on Proxies

If your system uses a proxy, i.e. `http_proxy`, `https_proxy` and `no_proxy` is defined. Then you can forward these using a vagrant plugin:
```
vagrant plugin install vagrant-proxyconf
```
For the user running Vagrant (e.g. if you need sudo to run vagrant also install the plugin with sudo).

Additionally, create the file `~/.vagrant.d/Vagrantfile` with the following contents:
```
Vagrant.configure("2") do |config|
  puts "proxyconf..."
  if Vagrant.has_plugin?("vagrant-proxyconf")
    puts "find proxyconf plugin !"
    if ENV["http_proxy"]
      puts "http_proxy: " + ENV["http_proxy"]
      config.proxy.http     = ENV["http_proxy"]
    end
    if ENV["https_proxy"]
      puts "https_proxy: " + ENV["https_proxy"]
      config.proxy.https    = ENV["https_proxy"]
    end
    if ENV["no_proxy"]
      config.proxy.no_proxy = ENV["no_proxy"]
    end
  end
end
```
