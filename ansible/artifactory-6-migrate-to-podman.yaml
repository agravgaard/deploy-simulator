---
- hosts: targets
  become: true
  vars:
    migrate: true
  pre_tasks:
    - name: extract java options from file
      slurp:
        src: "{{ artifactory_home }}/etc/default"
      register: artifactory_opts_file

    - name: extract nfs options from file
      slurp:
        src: /etc/fstab
      register: fstab_file

    - name: register username
      set_fact:
        artifactory_username: "{{ artifactory_opts_file['content'] \
                               | b64decode \
                               | regex_findall('ARTIFACTORY_USER=(.+)') \
                               | first }}"

    - name: debug username
      debug:
        var: artifactory_username

    - name: get group id
      user:
        name: "{{ artifactory_username }}"
      register: userdata

    - name: gid to groupname
      shell: >
        set -o pipefail &&
        getent group "{{ userdata.group }}"
        | cut -d: -f1
      register: group_result
      changed_when: false

    - name: register groupname
      set_fact:
        artifactory_groupname: "{{ group_result.stdout }}"

    - name: debug groupname
      debug:
        var: artifactory_groupname

  roles:
    - artifactory-datastore
    - podman

  tasks:
    - name: create directory for docker version of artifactory
      file:
        path: "{{ deploy_home }}"
        state: directory
        owner: "{{ artifactory_username }}"
        group: "{{ artifactory_groupname }}"
        mode: 01755

    - name: change permissions recursively on Artifactory directory
      file:
        path: "{{ artifactory_home }}"
        owner: "{{ artifactory_username }}"
        group: "{{ artifactory_groupname }}"
        recurse: true

    - name: extract uid and gid from passwd
      getent:
        database: passwd

    - name: create podman volumes
      containers.podman.podman_volume:
        name: "{{ item.key }}"
        options:
          - "type=nfs"
          - "device={{ item.value.address }}:{{ item.value.path }}"
          - "o=addr={{ item.value.address }},{{ item.value.options }}"
      with_dict: "{{ nfs_opts }}"

    - name: create nfs_mounts list
      set_fact:
        nfs_mounts: []

    - name: fill nfs_mounts list
      set_fact:
        nfs_mounts: >-
          {{ [ nfs_mounts, ( item.key + ':' + item.value.mount ) ] | flatten }}
      with_dict: "{{ nfs_opts }}"

    - name: stop and disable running artifactory
      service:
        name: artifactory
        state: stopped
        enabled: false

    - name: Start artifactory with podman
      containers.podman.podman_container:
        name: artifactory
        image: docker.bintray.io/jfrog/artifactory-oss:6.18.5
        user: "{{ art_uid }}:{{ art_gid }}"
        ports:
          - "8081:8081"
        dns:
          - 131.97.140.4
          - 131.97.143.4
        env:
          EXTRA_JAVA_OPTIONS: "{{ java_opts | default('-Xms512m -Xmx4g') }}"
        restart_policy: "always"
        volumes: "{{ [ artifactory_mounts, data_mounts ] | flatten }}"
        state: started
        # "generate_systemd" fails because it uses an unavailble flag, --format
      vars:
        # Regex skips the first option (assumed to be bullfrog)
        java_opts: "{{ artifactory_opts_file['content'] \
                    | b64decode \
                    | regex_findall('JAVA_OPTIONS=\".*?\\s(.+)\"') \
                    | first }}"
        art_uid: "{{ getent_passwd[artifactory_username].1 }}"
        art_gid: "{{ getent_passwd[artifactory_username].2 }}"
        artifactory_mounts:
          - "{{ deploy_home }}:/var/opt/jfrog/artifactory/etc:z"
          - "{{ artifactory_home }}/etc/db.properties:/var/opt/jfrog/artifactory/etc/db.properties:z"
          - "{{ artifactory_home }}/etc/binarystore.xml:/var/opt/jfrog/artifactory/etc/binarystore.xml:z"
          - "{{ artifactory_home }}/etc/security:/var/opt/jfrog/artifactory/etc/security:z"
          - "{{ artifactory_home }}/data:/var/opt/jfrog/artifactory/data:z"
          - "{{ artifactory_home }}/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar:/opt/jfrog/artifactory/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar:z"
        data_mounts: "{{ nfs_mounts \
                      | map('regex_replace', '^(.*)$', '\\1:rw') \
                      | list }}"
      register: podman_result

    - name: Generate systemd podman config
      shell:
        cmd: >
          podman generate systemd artifactory -n -f
          --restart-policy always
        chdir: /etc/systemd/system/
      when: podman_result.changed

    - name: Insert systemd podman dependencies
      blockinfile:
        block: |
          Wants=network.target
          After=network-online.target
        path: /etc/systemd/system/container-artifactory.service
        insertafter: "^Docu.+$"

    - name: Reload systemd daemon
      systemd:
        daemon_reload: true

    - name: start and enable podman artifactory
      service:
        name: container-artifactory
        state: started
        enabled: true
