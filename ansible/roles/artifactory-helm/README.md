# Deploy artifactory with Kubernetes (k8s)

See the `deploy.sh` file for an example of deployment, with a generated `hosts.ini` file.

## Paths assumed to exist on the remote host

Note: `deploy_home` and `artifactory_home` should be set in the `hosts.ini` file.

 - `/etc/fstab`, the file system mount configuration file (with entries for NFS drives)
 - `/etc/systemd/system/`, systemd is required for deploying the kubelet service.
 - `{{ deploy_home }}`, the installation directory for the new deployment, e.g. `/opt/jfrog/artifactory-k8s/`
 - `{{ artifactory_home }}`, the installation directory of Artifactory, e.g. `/opt/jfrog/artifactory/`
 - `{{ artifactory_home }}/etc/default`, the environment vaiable file for Artifactory, including Java options.
 - `{{ artifactory_home }}/etc/binarystore.xml`, the storage configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/db.properties`, the database configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/security/master.key`, access master key for Artifactory.
 - `{{ artifactory_home }}/etc/security/join.key`, access join key for Artifactory will be created if it doesn't exists.
 - `{{ artifactory_home }}/etc/data/`, the data directory for Artifactory.
 - `{{ artifactory_home }}/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar`, the database driver (currently hardcoded).
 - `/root/` will be used to store installs and in particular the `GOPATH`, kubernetes manifests and the `.kube/config`.
 - `/etc/module-load.d/`, the module load path for linux
 - `/etc/selinux/config`, for SELinux

## Other system requirements

We assume the OS to be "Red Hat Enterprise Linux 7.9", although it may work on other systems.
The package installation steps (i.e. installation of k8s, cri-o and their dependencies) are the most system-specific.

Python 3 must be installed on the remote host before the ansible scripts can run.

K8s is _not_ required. The ansible scripts will install k8s with the system package manager, e.g. `yum`.

## Upgrading Artifactory

Many paths in the `roles/helm_artifactory/templates/values.yaml.j2` are hardcoded to Artifactory 6.18.5 paths, if using another version all paths should be checked!
[See the upstream values file for reference](https://github.com/jfrog/charts/blob/master/stable/artifactory/values.yaml)
