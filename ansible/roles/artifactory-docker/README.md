# Deploy artifactory with Docker

See the `deploy.sh` file for an example of deployment, with a generated `hosts.ini` file.

## Paths assumed to exist on the remote host

Note: `deploy_home` and `artifactory_home` should be set in the `hosts.ini` file.

 - `/etc/fstab`, the file system mount configuration file (with entries for NFS drives)
 - `{{ deploy_home }}`, the installation directory for the new deployment, e.g. `/opt/jfrog/`
 - `{{ artifactory_home }}`, the installation directory of Artifactory, e.g. `/opt/jfrog/artifactory/`
 - `{{ artifactory_home }}/etc/default`, the environment vaiable file for Artifactory, including Java options.
 - `{{ artifactory_home }}/etc/binarystore.xml`, the storage configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/db.properties`, the database configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/security/`, the folder with access configuration for Artifactory, including `master.key` and `join.key`.
 - `{{ artifactory_home }}/etc/data/`, the data directory for Artifactory.
 - `{{ artifactory_home }}/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar`, the database driver (currently hardcoded).

## Other system requirements

We assume the OS to be "Red Hat Enterprise Linux 7.9", although it may work on other systems.
The package installation steps (i.e. installation of docker and its dependencies) are the most system-specific.

Python 3 must be installed on the remote host before the ansible scripts can run.

Docker is _not_ required. The ansible scripts will detect docker and update if version is too low or does not exist.
