# Deploy artifactory with yum

See the `deploy.sh` file for an example of deployment, with a generated `hosts.ini` file.

## Paths assumed to exist on the remote host

Artifactory is assumed to be installed in `/opt/jfrog/artifactory` already. But not necessarily by the package manager, i.e. `yum`.

## Other system requirements

We assume the OS to be "Red Hat Enterprise Linux 7.9", although it may work on other (yum-based) systems.
The package installation steps (i.e. installation of artifactory and its dependencies) are the most system-specific.
