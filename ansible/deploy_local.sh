#!/bin/bash

set -euo pipefail

export METHOD=${METHOD:-podman}
export MIGRATE=${MIGRATE:-1}
export ART_VERSION=${ART_VERSION:-7}
export RHEL_VERSION=${RHEL_VERSION:-8}
export RESERVED_MEMORY=${RESERVED_MEMORY:-31744}
export MASTER_KEY=${MASTER_KEY:-""}
export JOIN_KEY=${JOIN_KEY:-""}

export TARGET=localhost
export NO_INSTALL=0

./deploy.sh

unset TARGET
unset NO_INSTALL
