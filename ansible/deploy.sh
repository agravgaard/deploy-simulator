#!/bin/bash

set -euo pipefail

target="${TARGET:-rhel${RHEL_VERSION}}"
extra_vars=""
if [ "$MIGRATE" -ne 0 ]; then
  # shellcheck source=/dev/null
  if [[ "${target}" != "localhost" ]]; then
    . "../rhel${RHEL_VERSION}/env.sh"
  else
    . "../localhost/env.sh"
  fi
  JAVA_MEM=$((${RESERVED_MEMORY::-3} - 3))

  extra_vars="# Options only necessary when MIGRATE==false:
java_options=\"-server -Xms${JAVA_MEM}g -Xmx${JAVA_MEM}g -Xss256k -XX:+UseG1GC -XX:OnOutOfMemoryError='kill -9 %p' -Djruby.compile.invokedynamic=false -Dfile.encoding=UTF8 -Dartdist=zip -Dorg.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true -Djava.security.egd=file:/dev/./urandom\"
mssql_address=${MSSQL_ADDRESS}
mssql_port=${MSSQL_PORT}
database_name=${DATABASE_NAME}
sql_user=${SQL_INSTALL_USER}
sql_password=${SQL_INSTALL_USER_PASSWORD}
nfs_address=$(ip route get 1.2.3.4 | awk '{ print $7 }')"
else
  extra_vars="# Options only necessary when MIGRATE==true:
artifactory_home=/opt/jfrog/artifactory/"
fi

extra_vars_method=""
if [[ ${METHOD} == "k8s" ]]; then
  extra_vars_method="# Options only necessary when METHOD==k8s:
runtime=runc"
elif [[ ${METHOD} == "yum" ]]; then
  extra_vars_method="# Options only necessary when METHOD==yum:
package_name=jfrog-artifactory-oss
package_version=6.18.5
download_url_yum=\"https://bintray.com/jfrog/artifactory-rpms/rpm\"
download_url_manual=\"https://bintray.com/jfrog/artifactory/download_file?file_path=jfrog-artifactory-oss-6.18.5.zip\""
fi

key_vars=""
if [[ "${MASTER_KEY}" != "" ]]; then
  key_vars="master_key=${MASTER_KEY}"
fi
if [[ "${JOIN_KEY}" != "" ]]; then
  key_vars="${key_vars}
join_key=${JOIN_KEY}"
fi

ssh_opts=""
if [[ "${target}" != "localhost" ]]; then
  ssh_opts="ansible_ssh_user=vagrant
ansible_ssh_common_args='-F ./ssh_config'"
else
  ssh_opts="ansible_connection=local"
fi

cat <<EOF >./hosts.ini
[targets]
$target

[targets:vars]
ansible_python_interpreter=/usr/bin/python3
$ssh_opts
deploy_home=/opt/jfrog/artifactory-podman/
artifactory_username=cs-ws-s-artifactory
artifactory_groupname=cs_unix_stdgroup
artifactory_major_version=${ART_VERSION}
$extra_vars
$extra_vars_method
$key_vars
EOF

# YOUR DEPLOY SCRIPT HERE

if [[ "${METHOD}" == "podman" ]]; then
  # podman-compose requires python3+
  ansible-galaxy collection install containers.podman --upgrade
elif [[ "${METHOD}" == "docker" ]]; then
  # docker-compose requires python3+
  ansible-galaxy collection install community.docker --upgrade
elif [[ "${METHOD}" == "k8s" ]]; then
  # python3 may or may not be required
  ansible-galaxy collection install kubernetes.core --upgrade
  ansible-galaxy role install geerlingguy.kubernetes
fi

if [ "$MIGRATE" -eq 0 ]; then
  ansible-playbook "./artifactory-6-migrate-to-${METHOD}.yaml" -i ./hosts.ini --diff
elif [ "$NO_INSTALL" -eq 0 ]; then
  ansible-playbook "./artifactory-${ART_VERSION}-deploy-${METHOD}-no-install.yaml" -i ./hosts.ini --diff --ask-become-pass
else
  ansible-playbook "./artifactory-${ART_VERSION}-deploy-${METHOD}.yaml" -i ./hosts.ini --diff
fi
