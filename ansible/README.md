# Deploy Artifactory with METHOD

See the `deploy.sh` file for an example of deployment, with a generated `hosts.ini` file.
The variables `MIGRATE`, `METHOD`, `RHEL_VERSION` and `ART_VERSION` needs to be set for all methods.

### Support matrix

| METHOD:              | podman    | docker | yum    | k8s    |
| -------------------- | --------- | ------ | ------ | ------ |
| Migrate (RHEL6 only) | ✔         | ✔      | ✔      | ✔      |
| New                  | ✔ (6 & 7) | ✖      | ✖      | ✖      |

## For a new deployment
I.e. `MIGRATE=false`

The jinja2 template files:
 - `roles/mssql/templates/system.yaml.j2`
 - `roles/datastore/templates/binarystore.xml.j2`
 - `roles/datastore/templates/fstab.j2`
will be placed in `{{ deploy_home }}`.

## For a migration
I.e. `MIGRATE=true`

Paths are assumed to exist on the remote host:
See `roles/artifactory-*/README.md` for method specific instructions.

Note: `deploy_home` and `artifactory_home` should be set in the `hosts.ini` file.

 - `/etc/fstab`, the file system mount configuration file (with entries for NFS drives)
 - `/etc/systemd/system/`, systemd is required for deploying the service.
 - `{{ deploy_home }}`, the installation directory for the new deployment, e.g. `/opt/jfrog/artifactory-podman/`
 - `{{ artifactory_home }}`, the installation directory of Artifactory, e.g. `/opt/jfrog/artifactory/`
 - `{{ artifactory_home }}/etc/default`, the environment vaiable file for Artifactory, including Java options.
 - `{{ artifactory_home }}/etc/binarystore.xml`, the storage configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/db.properties`, the database configuration file for Artifactory.
 - `{{ artifactory_home }}/etc/security/`, the folder with access configuration for Artifactory, including `master.key` and `join.key`.
 - `{{ artifactory_home }}/etc/data/`, the data directory for Artifactory.
 - `{{ artifactory_home }}/tomcat/lib/mssql-jdbc-9.4.1.jre8.jar`, the database driver (currently hardcoded).

## Other system requirements

We assume the OS to be "Red Hat Enterprise Linux" 7 or 8, although it may work on other systems.
The package installation steps are generally the most system-specific.

Python 3 must exist on the remote host before the Ansible scripts can run.
