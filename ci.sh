#!/bin/bash

set -euo pipefail

export METHOD=${METHOD:-podman}
export MIGRATE=${MIGRATE:-1}
export NO_INSTALL=${NO_INSTALL:-1}
export ART_VERSION=${ART_VERSION:-7}
export RHEL_VERSION=${RHEL_VERSION:-8}
export RESERVED_MEMORY=${RESERVED_MEMORY:-31744}
export MASTER_KEY=${MASTER_KEY:-""}
export JOIN_KEY=${JOIN_KEY:-""}

reboot_vagrant () {
  cd "rhel${RHEL_VERSION}"
  vagrant reload  # halt (shutdown) + up (without provisioning)
  cd ..
}
test_artifactory () {
  cd "rhel${RHEL_VERSION}"
  vagrant ssh -c 'bash -s' <../wait_for_artifactory.sh
  vagrant ssh -c 'bash -s' <../test.sh
  cd ..
}

deploy_artifactory () {
  cd ansible
  ./deploy.sh
  cd ..
}

if [[ "${MIGRATE}" == 0 ]]; then
  echo "Test artifactory is up..."
  test_artifactory
fi

echo "Perfoming deployment with ${METHOD}..."
deploy_artifactory

echo "Test deploy..."
test_artifactory

echo "Check persistence after reboot..."
reboot_vagrant
test_artifactory

echo "Testing backup and idempotency..."
deploy_artifactory

test_artifactory
