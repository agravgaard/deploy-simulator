#!/bin/bash

if [[ -f $(pwd)/bullfrog/pom.xml ]]; then
  cd bullfrog || exit 4
  git apply ../bullfrog.patch
  mvn -T 2C clean install -DskipTests
  cd ./agent/dist/target/ || exit 4
  tar cvzf bullfrog.tar.gz ./*
  mv bullfrog.tar.gz ../../../../artifactory/
else
  echo "Bullfrog hasn't been cloned! Run:"
  echo "git submodule update --init --recursive"
fi
