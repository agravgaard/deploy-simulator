#!/bin/bash

set -euo pipefail

# We could also use kubeadm to check
if type "kubectl" >/dev/null; then
  ADDRESS=${ADDRESS:-$(ip route get 1.2.3.4 | awk '{ print $7 }')}
fi

ADDRESS=${ADDRESS:-localhost}
PORT=${PORT:-8081}
SUBDIR=${SUBDIR:-/artifactory/}
PROTOCOL=${PROTOCOL:-http}

echo "Smoke test"
echo "curl -SL $PROTOCOL://${ADDRESS}:${PORT}${SUBDIR}"
curl -SL "$PROTOCOL://${ADDRESS}:${PORT}${SUBDIR}"
echo ""

TEST_FILE=test_file.txt

if [[ ! -f $TEST_FILE ]]; then
  openssl rand -hex 32 >$TEST_FILE
  echo "Upload test"
  curl -uadmin:password -T $TEST_FILE "$PROTOCOL://${ADDRESS}:${PORT}${SUBDIR}example-repo-local/$TEST_FILE"
  echo ""
fi

echo "Download test"
curl -uadmin:password -o dl-$TEST_FILE "$PROTOCOL://${ADDRESS}:${PORT}${SUBDIR}example-repo-local/$TEST_FILE"
echo ""
cmp $TEST_FILE dl-$TEST_FILE || (cat dl-$TEST_FILE && exit 69)
