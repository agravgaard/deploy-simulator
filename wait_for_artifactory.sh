#!/bin/bash

set -uo pipefail

# We could also use kubeadm to check
if type "kubectl" >/dev/null; then
  ADDRESS=$(ip route get 1.2.3.4 | awk '{ print $7 }')
  sudo kubectl get all -A
fi

ADDRESS=${ADDRESS:-localhost}

PORT=8081

# Connect to server and get the version:
counter=1
errstatus=1
while [ $counter -le 24 ] && [ $errstatus = 1 ]; do
  echo Waiting for Artifactory to start...
  sleep 5s
  curl --max-time 10 -SL "${ADDRESS}:${PORT}/artifactory" | grep "title" | grep -v "Starting Up..."
  errstatus=$?
  ((counter++))
done
# Display error if connection failed:
if [ $errstatus = 1 ]; then
  echo "Error: Cannot connect to Artifactory:"
  curl --max-time 10 -SL "${ADDRESS}:${PORT}/artifactory"
  exit $errstatus
fi
